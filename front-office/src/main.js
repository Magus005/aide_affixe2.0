import { createApp } from 'vue'
import App from './v1.0/App.vue'
import router from './v1.0/routes/index';
import VueFuse from 'vue-fuse'

import Default from './v1.0/layouts/default.vue';


const myApp = createApp(App)

myApp.component("default-layout", Default);
myApp.use(VueFuse);
myApp.use(router);

myApp.config.productionTip = false;

myApp.mount('#app')