// CategoriesRepository.js

import Service from "./service";

const resource = "/categories";
export default {
    getAll() { return Service.get(`${resource}`); },
    findAllCategoryAndArticle() { return Service.get(`${resource}/getCategoriesAndArtistes`); },
    getOne(id) { return Service.get(`${resource}/${id}`); }
}