// ServiceFactory.js

import ArticlesRepository from './ArticlesRepository';
import CategoriesRepository from './CategoriesRepository';

const services = {
    categories: CategoriesRepository,
    articles: ArticlesRepository
};

export const ServiceFactory = {
    get: name => services[name]
}