// ArticlesRepository.js

import Service from "./service";

const resource = "/articles";
export default {
    getAll() { return Service.get(`${resource}`); },
    getArticleByCategory(categoryId) { return Service.get(`${resource}/byCategory/${categoryId}`); },
    getOne(id) { return Service.get(`${resource}/${id}`) }
}