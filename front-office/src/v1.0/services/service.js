// Service.js

import axios from 'axios';
const config = require('../config/environment/index')

const baseDomain = config.api;
const baseURL = `${baseDomain}api`;

export default axios.create({
    baseURL
})