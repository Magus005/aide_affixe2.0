import { createWebHistory, createRouter } from "vue-router";



const routes = [{
        path: "/",
        name: "category",
        component: () =>
            import ("../pages/category/category.vue")
    },
    {
        path: "/category/:id",
        name: "detailCategory",
        component: () =>
            import ("../pages/detail-category/detailCategory.vue")
    },
    {
        path: "/article/:id",
        name: "article",
        component: () =>
            import ("../pages/article/article.vue")
    },
]


const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;