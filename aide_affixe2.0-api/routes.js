/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';

export default function(app) {
    // routes
    app.use('/api/categories', require('./api/category'));
    app.use('/api/articles', require('./api/article'));

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|public)/*').get(errors[404]);
}