import { v4 as uuidv4 } from 'uuid';
var cluster = require("../../config/database").cluster;
var bucket_categories = require("../../config/database").bucket_categories;
var bucket_articles = require("../../config/database").bucket_articles;
var config = require('../../config/environment')

var collection = bucket_categories.defaultCollection();

function Category() {}

Category.findAll = async function(callback) {
    try {
        var statement = `SELECT META(category).id, category.name, category.picture, category.state, category.type
            FROM \`${config.database.bucket_categories}\` AS category WHERE category.type=$1`;

        const options = { parameters: ['category'] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows);
    } catch (error) {
        return callback(error, null);
    }
}

Category.findAllCategoryAndArticle = async function(callback) {
    try {
        let categories = [];
        var statement = `SELECT META(article).id, article.name, article.content, article.listCategory, article.state, article.type 
        FROM \`${config.database.bucket_articles}\` AS article WHERE $1 IN article.listCategory`;

        this.findAll(function(error, result) {
            if (error) {
                return error;
            }
            result.forEach((category, index) => {
                const options = { parameters: [category.id] };
                cluster.query(statement, options).then((value) => {
                    categories.push({ category, articles: value.rows })
                    if (categories.length == result.length) {
                        return callback(null, categories);
                    }
                });
            });
        })
    } catch (error) {
        return callback(error, null);
    }
}


Category.findById = async function(id, callback) {
    try {
        var statement = `SELECT META(category).id, category.name, category.picture, category.state, category.type 
            FROM \`${config.database.bucket_categories}\` AS category USE KEYS $1 WHERE category.type=$2`;

        const options = { parameters: [`${id}`, 'category'] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}


Category.create = async function(data, callback) {
    try {
        var category = { "name": `${data.name}`, "picture": `${data.picture}`, "type": "category", "state": "active" };
        var id = data.id ? data.id : uuidv4();
        var statement = `INSERT INTO \`${config.database.bucket_categories}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as categoryid, *`;
        const options = { parameters: [`${id}`, category] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}


Category.update = async function(data, categoryToUpdate, callback) {
    try {
        const { id } = data.params;
        let category = {
            "name": data.body.name ? data.body.name : categoryToUpdate.name,
            "picture": data.body.picture ? data.body.picture : categoryToUpdate.picture,
            "type": "category",
            "state": "active"
        }
        var statement = `UPSERT INTO \`${config.database.bucket_categories}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as categoryid, *`;
        const options = { parameters: [`${id}`, category] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Category.cancel = async function(id, data, callback) {
    try {
        data.state = "deactive";
        var statement = `UPSERT INTO \`${config.database.bucket_categories}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as categoryid, *`;
        const options = { parameters: [`${id}`, data] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Category.destroy = async function(id, callback) {
    try {
        var statement = `DELETE FROM \`${config.database.bucket_categories}\` AS category USE KEYS $1 
            WHERE category.type=$2`;
        const options = { parameters: [`${id}`, 'category'] };
        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

module.exports = Category;