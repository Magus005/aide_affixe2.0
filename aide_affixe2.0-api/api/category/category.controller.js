import Category from './category.model';
const config = require('../../config/environment/index');



export function index(req, res) {
    Category.findAll(function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function getCategoriesAndArtistes(req, res) {
    Category.findAllCategoryAndArticle(function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function show(req, res) {
    const { id } = req.params;
    Category.findById(id, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function create(req, res) {
    console.log('req.body', req.body);
    console.log('req.file', req.file);
    if (!req.body.name) {
        return res.status(400).json("Please provide complete details");
    }

    let picture = `${config.pathFile}/${req.file.filename}`
    Category.create({ "name": req.body.name, "picture": picture }, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export async function update(req, res) {
    const { id } = req.params;
    let categoryToUpdate;
    if (!req.body.name && !req.body.picture) {
        return res.status(400).json("Please provide complete details");
    }
    searchCategory(id, function(result) {
        categoryToUpdate = result;
        if (!categoryToUpdate) {
            return res.status(204).json(`no content`);
        }
        Category.update(req, categoryToUpdate, function(error, result) {
            if (error) {
                return res.status(400).send(error);
            }
            res.send(result);
        })
    });
}

export async function cancel(req, res) {
    const { id } = req.params;
    let categoryToCancel;
    if (!req.body.name && !req.body.picture) {
        return res.status(400).json("Please provide complete details");
    }
    searchCategory(id, function(result) {
        categoryToCancel = result;
        if (!categoryToCancel) {
            return res.status(204).json(`no content`);
        }
        Category.cancel(id, categoryToCancel, function(error, result) {
            if (error) {
                return res.status(400).send(error);
            }
            res.send(result);
        })
    })
}

export function remove(req, res) {
    const { id } = req.params;
    Category.destroy(id, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

function searchCategory(id, callback) {
    Category.findById(id, function(error, result) {
        if (error) {
            return error;
        }
        return callback(result);
    });
}