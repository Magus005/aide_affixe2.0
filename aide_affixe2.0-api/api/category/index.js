var express = require('express');
const upload = require('../../components/multer');
var controller = require('./category.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/getCategoriesAndArtistes', controller.getCategoriesAndArtistes);
router.get('/:id', controller.show);
router.post('/', upload.single('category'), controller.create);
router.put('/:id', controller.update);
router.delete('/cancel/:id', controller.cancel);
router.delete('/:id', controller.remove);

module.exports = router;