import { v4 as uuidv4 } from 'uuid';
var cluster = require("../../config/database").cluster;
var bucket_articles = require("../../config/database").bucket_articles;
var config = require('../../config/environment')

var collection = bucket_articles.defaultCollection();

function Article() {}

Article.findAll = async function(callback) {
    try {
        var statement = `SELECT META(article).id, article.name, article.content, article.listCategory, article.state, article.type 
            FROM \`${config.database.bucket_articles}\` AS article WHERE article.type=$1`;

        const options = { parameters: ['article'] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows);
    } catch (error) {
        return callback(error, null);
    }
}


Article.findById = async function(id, callback) {
    try {
        var statement = `SELECT META(article).id, article.name, article.content, article.listCategory, article.state, article.type 
            FROM \`${config.database.bucket_articles}\` AS article USE KEYS $1 WHERE article.type=$2`;

        const options = { parameters: [`${id}`, 'article'] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Article.findByCategory = async function(categoryId, callback) {
    try {
        var statement = `SELECT META(article).id, article.name, article.content, article.listCategory, article.state, article.type 
            FROM \`${config.database.bucket_articles}\` AS article WHERE article.type=$2 AND $1 IN article.listCategory`;

        const options = { parameters: [`${categoryId}`, 'article'] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows);
    } catch (error) {
        return callback(error, null);
    }
}

Article.create = async function(data, callback) {
    try {
        var article = {
            "name": `${data.name}`,
            "content": `${data.content}`,
            "listCategory": data.listCategory,
            "type": "article",
            "state": "active"
        };
        var id = data.id ? data.id : uuidv4();
        var statement = `INSERT INTO \`${config.database.bucket_articles}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as articleid, *`;
        const options = { parameters: [`${id}`, article] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Article.update = async function(data, articleToUpdate, callback) {
    try {
        const { id } = data.params;
        let article = {
            "name": data.body.name ? data.body.name : articleToUpdate.name,
            "content": data.body.content ? data.body.content : articleToUpdate.content,
            "listCategory": data.body.listCategory ? data.body.listCategory : articleToUpdate.listCategory,
            "type": "article",
            "state": "active"
        }
        var statement = `UPSERT INTO \`${config.database.bucket_articles}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as articleid, *`;
        const options = { parameters: [`${id}`, article] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Article.cancel = async function(id, data, callback) {
    try {
        data.state = "deactive";
        var statement = `UPSERT INTO \`${config.database.bucket_articles}\` (KEY, VALUE) VALUES($1, $2)
            RETURNING META().id as articleid, *`;
        const options = { parameters: [`${id}`, data] };

        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}

Article.destroy = async function(id, callback) {
    try {
        var statement = `DELETE FROM \`${config.database.bucket_articles}\` AS article USE KEYS $1 
            WHERE article.type=$2`;
        const options = { parameters: [`${id}`, 'article'] };
        let results = await cluster.query(statement, options);
        return callback(null, results.rows[0]);
    } catch (error) {
        return callback(error, null);
    }
}


module.exports = Article;