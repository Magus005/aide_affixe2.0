var express = require('express');
var controller = require('./article.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/byCategory/:id', controller.getArticleByCategory);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/cancel/:id', controller.cancel);
router.delete('/:id', controller.remove);


module.exports = router;