import Article from './article.model';



export function index(req, res) {
    Article.findAll(function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function show(req, res) {
    const { id } = req.params;
    Article.findById(id, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function getArticleByCategory(req, res) {
    const { id } = req.params;
    Article.findByCategory(id, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export function create(req, res) {
    if (!req.body.name || !req.body.content || !req.body.listCategory[0]) {
        return res.status(400).json("Please provide complete details");
    }
    Article.create(req.body, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

export async function update(req, res) {
    const { id } = req.params;
    let articleToUpdate;
    if (!req.body.name && !req.body.content && !req.body.listCategory[0]) {
        return res.status(400).json("Please provide complete details");
    }
    searchArticle(id, function(result) {
        articleToUpdate = result;
        if (!articleToUpdate) {
            return res.status(204).json(`no content`);
        }
        Article.update(req, articleToUpdate, function(error, result) {
            if (error) {
                return res.status(400).send(error);
            }
            res.send(result);
        })
    });
}

export async function cancel(req, res) {
    const { id } = req.params;
    let articleToCancel;
    if (!req.body.name && !req.body.picture) {
        return res.status(400).json("Please provide complete details");
    }
    searchArticle(id, function(result) {
        articleToCancel = result;
        if (!articleToCancel) {
            return res.status(204).json(`no content`);
        }
        Article.cancel(id, articleToCancel, function(error, result) {
            if (error) {
                return res.status(400).send(error);
            }
            res.send(result);
        })
    })
}

export function remove(req, res) {
    const { id } = req.params;
    Article.destroy(id, function(error, result) {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(result);
    })
}

function searchArticle(id, callback) {
    Article.findById(id, function(error, result) {
        if (error) {
            return error;
        }
        return callback(result);
    });
}