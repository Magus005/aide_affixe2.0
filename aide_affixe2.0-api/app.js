import createError from 'http-errors';
import path from 'path';
import express from 'express';
import bodyparser from 'body-parser';
import logger from 'morgan';
import registerRoutes from './routes';
const config = require('./config/environment/index');
import cors from 'cors';


var app = express();


//CORS
app.use(cors());


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.get("/public/", express.static(path.join(__dirname, "./public")));

// Routes
registerRoutes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

const PORT = process.env.PORT || config.port;

const server = app.listen(PORT, config.ip, function() {
    console.log(`Express server listening on ${PORT} ip: ${config.ip}, in ${app.get('env')} mode`);
})

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;