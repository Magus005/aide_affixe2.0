var couchbase = require("couchbase");
const config = require('./environment/index');


const cluster = new couchbase.Cluster(config.database.server, {
    username: config.database.username,
    password: config.database.password,
});
const bucket_categories = cluster.bucket(config.database.bucket_categories);
const bucket_articles = cluster.bucket(config.database.bucket_articles);
module.exports = { bucket_articles, bucket_categories, cluster };