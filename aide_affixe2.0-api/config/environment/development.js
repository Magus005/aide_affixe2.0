module.exports = {
    // PostgreSQL
    database: {
        server: "127.0.0.1:8091",
        bucket_categories: "categories",
        bucket_articles: "articles",
        username: "admin",
        password: "passer123"
    },
    // Path File
    pathFile: 'http://localhost:5010',
    // Server Port
    port: 5010,
    // Server IP
    ip: '127.0.0.1',
    corsDomain: '*',
    env: 'development'
};