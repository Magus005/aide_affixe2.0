const _ = require('lodash');

var all = {
    env: process.env.NODE_ENV || 'development',
    corsDomain: '*',
    port: process.env.PORT || 5010
};


module.exports = _.merge(
    all,
    require(`./${process.env.NODE_ENV}.js`) || {});