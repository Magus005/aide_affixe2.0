# Aide_Affixe2.0
Aide Affixe 2.0 Clone

## Les Endpoints

### Categories
- GET http://localhost:5010/api/categories/
- GET http://localhost:5010/api/categories/getCategoriesAndArtistes
- GET http://localhost:5010/api/categories/:id
- POST http://localhost:5010/api/categories/
- PUT http://localhost:5010/api/categories/:id
- DELETE http://localhost:5010/api/categories/cancel/:id
- DELETE http://localhost:5010/api/categories/:id

### Articles
- GET http://localhost:5010/api/articles/
- GET http://localhost:5010/api/articles/:id
- GET http://localhost:5010/api/articles/byCategory/:id
- POST http://localhost:5010/api/articles/
- PUT http://localhost:5010/api/articles/:id
- DELETE http://localhost:5010/api/articles/cancel/:id
- DELETE http://localhost:5010/api/articles/:id


## Projets
### Back-office: Angular
- ng serve
### Front-office: Vuecli 3
- npm run serve
### API: NodeJS Express
- npm run start
### Couchbase
- Restore: cbrestore ~/backup-aide-affixe2 http://localhost:8091 -b bucket-articles
- Restore: cbrestore ~/backup-aide-affixe2 http://localhost:8091 -b bucket-categories

## Contact
### Madiara Gassama <madiara.gassama005@gmail.com>
### Madiara Gassama <gassama.madiara@gmail.com>
