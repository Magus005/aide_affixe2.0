import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddArticleComponent } from './article/add-article/add-article.component';
import { ArticlesListComponent } from './article/articles-list/articles-list.component';
import { AddCategoryComponent } from './category/add-category/add-category.component';
import { CategoriesListComponent } from './category/categories-list/categories-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'categories', pathMatch: 'full' },
  { path: 'categories', component: CategoriesListComponent },
  { path: 'categories/add', component: AddCategoryComponent },
  { path: 'articles', component: ArticlesListComponent },
  { path: 'articles/add', component: AddArticleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
