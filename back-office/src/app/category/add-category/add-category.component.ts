import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment  } from 'src/environments/environment';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {
  categoryForm: FormGroup;
  data: any = {};


  constructor(private router: Router, private formBuilder: FormBuilder, private cd: ChangeDetectorRef, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.categoryForm = this.formBuilder.group({
      category: [''],
      name: ['', Validators.required ]
    });
  }


  selectPicture(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.categoryForm.patchValue({
      category: file
    });
    this.categoryForm.get('category').updateValueAndValidity();
  }




  addCategory() {
    console.log('formdata category', this.categoryForm.get('category').value);
    console.log('formdata name', this.data.name);

    console.log('formdata', this.categoryForm.value);

    const formData: any = new FormData();
    formData.append('name', this.categoryForm.get('name').value);
    formData.append('category', this.categoryForm.get('category').value);

    this.http.post(`${environment.apiUri}categories`, formData).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/categories']);
      },
      (error) => console.log(error)
    );
  }


}
