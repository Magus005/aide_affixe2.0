import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {
  allCategories: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.categoriesList();
  }


  categoriesList() {
    this.http.get(`${environment.apiUri}categories`).subscribe(
      (response) => {
        console.log(response);
        this.allCategories = response;
      },
      (error) => console.log(error)
    );
  }

  gotoAddCategory() {
    window.location.href = '/categories/add';
  }
}
