import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent implements OnInit {
  articleForm: FormGroup;
  data: any = {};
  allCategories: any = [];
  selectedCategories: any = [];
  loadListCategories = false;

  constructor(private router: Router, private formBuilder: FormBuilder, private elRef: ElementRef, private http: HttpClient) { }

  ngOnInit(): void {
    this.articleForm = this.formBuilder.group({
      content: ['', Validators.required],
      name: ['', Validators.required ]
    });
    this.categoriesList();
  }

  categoriesList() {
    this.loadListCategories = false;
    this.http.get(`${environment.apiUri}categories`).subscribe(
      (response) => {
        console.log(response);
        this.allCategories = response;
        this.loadListCategories = true;
        console.log('loadListCategories : ', this.loadListCategories);
      },
      (error) => console.log(error)
    );
  }

  addArticle() {
    const listCategoriesSelected = this.elRef.nativeElement.querySelector('.filter-option-inner-inner').innerHTML.split(', ');
    this.allCategories.forEach(category => {
      console.log(listCategoriesSelected.includes(category.name));
      console.log(category.name);
      console.log(category.id);
      console.log(listCategoriesSelected);
      if (listCategoriesSelected.includes(category.name)) {
        this.selectedCategories.push(category.id);
      }
    });
    console.log(this.selectedCategories);
    console.log(this.data.name);
    console.log(this.data.content);

    const artist = {
      name: this.data.name,
      content: this.data.content,
      listCategory: this.selectedCategories
    };

    this.http.post(`${environment.apiUri}articles`, artist).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/articles']);
      },
      (error) => console.log(error)
    );
  }

}
