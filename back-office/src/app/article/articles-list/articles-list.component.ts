import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {
  allArticles: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.articlesList();
  }

  articlesList() {
    this.http.get(`${environment.apiUri}articles`).subscribe(
      (response) => {
        console.log(response);
        this.allArticles = response;
      },
      (error) => console.log(error)
    );
  }

  gotoAddArticle() {
    window.location.href = '/articles/add';
  }

}
